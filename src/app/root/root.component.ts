import { Component, OnInit } from '@angular/core';
import { RootService } from  './root.service';
import { AuthService } from '../auth.service';
@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.css']
})
export class RootComponent implements OnInit {

  constructor(private rootService : RootService,private auth: AuthService) { }

  ngOnInit() {
  }
}
