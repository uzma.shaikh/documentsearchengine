import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RootService {

  constructor(private http: HttpClient) { }

  getAPIData(): Observable<Object>{
   return this.http.get('https://jsonplaceholder.typicode.com/users')
  }
  
  postAPIData({'Query': search}){
    return this.http.post('http://localhost:8081/api/postData', {'Query' : search }) // posting query from user to node server
  }

  readMoreAPIData({'DocId' : data}){
  	return this.http.post('http://localhost:8081/api/readMore/', {'DocId' : data }) // posting query from user to node server
  }

}
