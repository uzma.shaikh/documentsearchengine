import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AppRoutingModule } from './app-routing.module';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { AuthService } from './auth.service';
import { RootComponent } from './root/root.component';
import { SearchComponent } from './search/search.component';
import { RootService } from './root/root.service';
import { FormsModule} from '@angular/forms'
import { from } from 'rxjs';
import { HttpClientModule} from "@angular/common/http";
import { FooterComponent } from './footer/footer.component';
import { environment, config } from 'src/environments/environment';

@NgModule({
  declarations: [
    UserProfileComponent,
    RootComponent,
    SearchComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(config),
    AngularFirestoreModule,
    AngularFireAuthModule,
    FormsModule,
    HttpClientModule,
    
  ],
  providers: [RootService],
  bootstrap: [RootComponent]
})
export class AppModule { }
