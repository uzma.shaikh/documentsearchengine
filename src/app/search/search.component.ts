import { Component, OnInit } from '@angular/core';
import { RootService } from '../root/root.service';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  data :any;
  moreData: any;
  constructor(private rootService: RootService) {  }

  ngOnInit() {
  
  }

  //show variable handles the visibility for the division containing 
  //abstract of the documents
  show: boolean = true;

  //showSearch variable handles the visibility of search bar 
  // showSearch : True => search bar is visible 
  // read : False => search bar is not visible 
  showSearch: boolean = true;

  // read handles if document is in abstracted form or full state 
  // read : True => full document is visible 
  // read : False => abstract of document is visisble  
  read: boolean = false;

  readLess(){
    this.showSearch = !this.showSearch
    this.read = !this.read;
  }

  // Get the documentID and redirect the request to node server to get 
  // the content of full document 
  readMore(data){
    console.log(data)
    this.rootService.readMoreAPIData({'DocId': data}).subscribe((response)=>{
      console.log('response from post data is', response)
      this.moreData=response;
      this.showSearch = !this.showSearch
      this.read = !this.read;
    }, (error)=>{
      console.log('error during post is ', error);
    })

  }

  // Get the query and redirect the request to node server to get 
  // the response ie the relevent documents
  onClick(FormData){          
    this.rootService.postAPIData({'Query': FormData}).subscribe((response)=>{   // gives (FormData i.e. query) as response
      console.log('response from post data is ', response);
      this.data=response;
    },(error)=>{
      console.log('error during post is ', error)
    })
  }



}
