git clone https://gitlab.com/abhishel/docsearch.git
cd docsearch
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master


cd existing_folder
git init
git remote add origin https://gitlab.com/abhishel/docsearch.git
git add .
git commit -m "Initial commit"
git push -u origin master


cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/abhishel/docsearch.git
git push -u origin --all
git push -u origin --tags