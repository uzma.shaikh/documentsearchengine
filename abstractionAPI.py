from flask import Flask, request
import json
import time
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from io import StringIO
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np
import pandas as pd

 
app = Flask(__name__)

def text_similarity(sent, data):
    """
    takes input text and query, creates tf-idf representations, return top 3 similar sentences
    """
    out = []
    message = "success"
    success = True
    #tf-idf vectorizer
    tfidf_vectorizer = TfidfVectorizer()
    
    # adding the input query in the data for tf-idf representation 
    data.insert(0,sent)

    tfidf_matrix = tfidf_vectorizer.fit_transform(data)
    sim_mat = cosine_similarity(tfidf_matrix[0:1], tfidf_matrix)[0]
    
    # creating dataframe of the scores
    df = pd.DataFrame(list(zip(data, sim_mat)), columns =['sent', 'sim'])

    # sorting and selecting top candidates
    try:
        df.sort_values("sim", axis = 0, ascending = False, inplace = True, na_position ='last') 
        # sentence at index 0 is the original query, so ignoring it
        if (len(df['sent'].tolist())>3):
            out = df['sent'][1:4].tolist()
        else:
            out = df['sent'][1:].tolist()

    except Exception as e:
        print("Error: ", str(e))
        message = "Error: " + str(e)
        success = False
    
    return out, message, success



def split_para(text, min_words = 4):
    """
    take input text and return list of sentence(mostly representing paragraphs)
    conditions made on minimum number of words needs to be included
    """
    para_txt = []
    message = "success"
    success = True
    try:
        for s in text.split('.'):
            txt = s.strip()

            if len(s.split())>min_words:
                para_txt.append(s)
    except Exception as e:
        message = "Error: " + str(e)
        success = False

    return para_txt, message, success



 
@app.route('/')
def index():
    return "Flask server"
 
@app.route('/postdata', methods = ['POST'])
def postdata():

    data = request.get_json()
    text = data['content']
    query = data['query']


    print(data)
    
    out = []
    success = True
    message = "Success in execution"
    min_words = 3
    try:
        data, message, success = split_para(text,min_words)
        if success:
            out, message, success = text_similarity(query, data)
    except Exception as e:
        success = False
        message = "Error: " + str(e)

    # print(out)
    return json.dumps(out)
 
if __name__ == "__main__":
    app.run(port=5000, debug=True)