const express=require('express')
const app=express()
const bodyParser=require('body-parser')
const cors=require('cors');
var request = require("request")
const { Client } = require('@elastic/elasticsearch')
const client = new Client({ node : 'http://localhost:9200' })


app.use("*",cors());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.post('/api/readMore', bodyParser.json(), (req, res) =>{
	// Takes the documentId of document to be searched and send back 
	// to the frontend 
	var docId = req.body.DocId
	client.search({
		index : 'documentstorage',
		body : {
			query : {
				match : {
					documentid : docId
				}
			}
		}
	}, function(err, content){
		if(err){
			console.log(err)
		} else {

			// storing the full content of document in relatedContent
			relatedContent = []
			content.body.hits.hits.forEach(function (hit) {
			    relatedContent.push(hit);
			});

			ans = [];
			//sending only the first document to render
			ans.push(relatedContent[0]);
			res.json(ans)
		}
	})
})

app.post('/api/postData',bodyParser.json(), (req, res) => {
	// Takes the query and get the most relevent documents from the elasticsearch
	// server running at port 9200
	// getting the query to be searched 
    var askedQuery = req.body.Query.data;
	client.search({
		index : 'documentstorage',
		body : {
			query : {
				// matched the document using its document heading and its content 
				// weight of heading is 2 times of that content 
				multi_match : {
					query : askedQuery,
					fields : ["documentcontent", "documentheading^2"],
					minimum_should_match : "30%",
					fuzziness: "2"
				}
			}
		}
	}, function repeatUntillGetAll(err, relatedDocs){
		if(err){
			console.log(err)
		} else {
			// if the returned docs does not have any match 
			if(relatedDocs.length == 0){
				res.json({})
			} else {

				// storing all the matched document
				relatedRecords = []
				relatedDocs.body.hits.hits.forEach(function (hit) {
					// showing only the first few line of the document 
					hit['_source']['documentcontent'] = hit['_source']['documentcontent'].substr(0, 400);
				    relatedRecords.push(hit);
				});

				var abstractdoc = relatedRecords[0]._source.documentcontent;
				var data = { content : abstractdoc, query : askedQuery }
				var options = {
					method: 'POST',
			        uri: 'http://127.0.0.1:5000/postdata',
			        body: data,
			        json: true 
				};

				//getting the abstract most relevent document 
				request(options, function(err, resp, body){
					if(err){
						console.log('err');
					} else {
						// replacing the the content of most relevant document with the abstract of it 
						relatedRecords[0]._source.documentcontent = body;
						res.json(relatedRecords)
					}
				})
			}
			
		}
	});
})


//starting the server 
app.listen(8081,()=>console.log('Example app listening on port 8081!'))